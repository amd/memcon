# Memcon (Memory and Confidence)
This repository acts as a primer for an EEG experiment revolving around confidence judgments in a working memory task. The experiment will present a sequence of one to three everyday items to the participant that they have to memorize for a broef period. After that, they are tasked to rotate one of the objects back to its memorized orientation. Then on of two possible tasks will follow, either (1) a confidence judgment task, where they have to indicate their confidence using a wedge made of the test item such that they are certain that the original orientation lies within that wedge, or (2) a visual reproduciton task, in which participants have to reproduce the angular size of a target wedge on top of the screen with a wedge presented in the center.
The idea is that until the confidence task, participants should maintain the target item as well as possible and are able to judge their own confidence, which is indicated by a wedge of the same object centered around their reponse orientation. As this wedge uses the same object that shear apart while spanning the width of the wedge, we try to nudge the participants towards a more `visual` and viridical representation of the object and its uncertainty.
We simultaneously record eye tracking and EEG data with the aim to understand 

1. How neural uncertainty is represented in the brain on a temporally resolved manner

2. How participants use this representation of uncertainty and potentially information about other processes, such as attentiveness, to inform their sense of confidence

This primer is designed to run in the EEG lab at the MPI for Human Development using an Eyelink 1000 and an actiCap, Brain Products EEG System, but with reasonable changes should be able to run on different systems as well.

# Installation
1. Clone this repository to your local machine
1. Download miniconda
2. Run `conda install mamba -c conda-forge`
3. Run the following commands from the root of this repository
```
mamba env create -f environment.yml
conda activate memcon_experiment
pre-commit install
```
