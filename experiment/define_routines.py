"""Define routines for the experiment flow."""

import datetime
import json
import os
from pathlib import Path

import numpy as np
from define_eyetracking import (
    DummyEyeLink,
    check_drift,
    start_eye_recording,
    stop_eye_recording,
)
from define_settings import (
    EXPECTED_FPS,
    FIXSTIM_OFF_FRAMES,
    IMAGE_SIZE_DVA,
    KEYLIST_DICT,
    MAX_REWARD,
    N_FEEDBACK_BLOCK,
    NTRIALS,
    REWARD_CONSTANT,
    ROT_ACCELERATION,
    TEXT_HEIGHT_DVA,
    WAIT_RESPONSE_PROBE_FRAMES,
    max_wait_response,
)
from define_stimuli import (
    get_central_text_stim,
    get_confidence_wedge,
    get_ctrl_target_wedge,
    get_exp_text_stim,
    get_image_stim,
)
from define_ttl import get_ttl_dict, send_trigger
from psychopy import core, gui
from utils import (
    calculate_performance,
)


def display_iti(win, min_ms, max_ms, fps, rng, trigger_kwargs):
    """Display and return an inter-trial-interval.

    Parameters
    ----------
    win : psychopy.visual.Window
        The psychopy window on which to draw the stimuli.
    min_ms, max_ms :  int
        The minimum and maximum inter-trial-interval time in milliseconds.
    fps : int
        Refreshrate of the screen.
    rng : np.random.Generator
        The random number generator object based on which to
        generate the inter-trial-intervals.
    trigger_kwargs : dict
        Contains keys ser, tk, and byte. To be passed to the send_trigger
        function.

    Returns
    -------
    iti_ms : int
        the inter-trial-interval in milliseconds.
    """
    low = int(np.floor((min_ms / 1000) * fps))
    high = int(np.ceil((max_ms / 1000) * fps))
    iti_frames = rng.integers(low, high + 1)

    # if we want to send a trigger
    win.callOnFlip(send_trigger, **trigger_kwargs)
    for _ in range(iti_frames):
        win.flip()

    iti_ms = (iti_frames / fps) * 1000
    return iti_ms


def display_trial_images(
    win, image_frames, isi_frames, image_stims, trigger_kwargs_list
):
    """Display the image stimuli on the window. For each image, present it first
    followed by a pause with a blank screen.

    Parameters
    ----------
    win : psychopy.visual.Window
        The psychopy window on which to draw the stimuli.
    image_frames, isi_frames : int
        The number of frames (win flips) to show an image, and blank ISI screen.
    image_stims : list
        Contains the psychopy stimuli for the images.
    trigger_kwargs_list : list of dicts
        Each entry in the list corresponds to an image in the sequence.
    """
    # for each stimulus in list
    for i_image, image in enumerate(image_stims):
        # send trigger if we have them in list
        if trigger_kwargs_list is not None:
            trigger_kwargs = trigger_kwargs_list[i_image]
            win.callOnFlip(send_trigger, **trigger_kwargs)

        # show image stimulus for certain amount of frames
        for _ in range(image_frames):
            image.draw()
            win.flip()

        # here we display the inter-stimulus interval
        for _ in range(isi_frames):
            win.flip()


def display_probe_response(win, kb, image_data, probe_ori, trigger_kwargs_list):
    """Display the probe stimulus we choose and let participants adjust the orientation. For this
    we only allow the keys from the KEYLIST_DICT as inputs. For each frame we check if a key has
    been pressed and released and count each consecutive frame to adjust the acceleration of the
    item rotation. For this we take the sum over all keys that do the same, e.g. 'a' and 'left',
    so that both left and right-handed participants can easily do the task.

    Parameters
    ----------
    win :
    trigger_kwargs_list : list of dicts
        Each entry in the list corresponds to a certain action flagged with a trigger.

    Returns
    -------
    probe_stim.ori : float
        final orientation
    ori_keylog : list
        the list of key presses
    """
    # add leading singleton axis for iteration
    # image_data = image_data[np.newaxis, ...]

    probe_stim = get_image_stim(
        win,
        image_data,
        probe_ori,
        size=IMAGE_SIZE_DVA,
    )[0]

    # display probe for a fixed time until we allow user input
    trigger_kwargs = trigger_kwargs_list[0]
    win.callOnFlip(send_trigger, **trigger_kwargs)
    for _ in range(FIXSTIM_OFF_FRAMES):
        probe_stim.draw()
        win.flip()

    # get list of allowed keys
    key_list = [key for action_list in KEYLIST_DICT.values() for key in action_list]

    # keep a tap of which keys are pressed down
    key_down = {key: 0 for key in key_list}
    # send trigger when a key is pressed for the first time
    first_key_down = False
    # initialize list of key presses
    ori_keylog = []

    # let user change probe orientation with key presses
    for _ in range(WAIT_RESPONSE_PROBE_FRAMES):
        # reset movement
        dirsign = 0

        # get, but do not clear, pressed keys
        keys = kb.getKeys(key_list, waitRelease=False, clear=False)
        # get and clear key when released
        keys_released = kb.getKeys(key_list, waitRelease=True, clear=True)

        # print pressed and released keys to command line
        if keys:
            # count frames where key is pressed down
            for k in keys:
                key_down[k.name] += 1
                # send trigger when key is pressed the first time
                if not first_key_down:
                    trigger_kwargs = trigger_kwargs_list[1]
                    send_trigger(**trigger_kwargs)
                    first_key_down = True
                # append single key press to key log
                if key_down[k.name] == 1:
                    ori_keylog.append(["pressed: ", [(k.rt, k.name) for k in keys]])
        if keys_released:
            # reset key frame count
            for k in keys:
                # append single key press to key log
                if key_down[k.name] != 0:
                    ori_keylog.append(
                        ["released: ", [(k.rt, k.name) for k in keys_released]]
                    )
                key_down[k.name] = 0

        # if any "up" key is pressed, we can end the loop here
        if any(key_down[key] > 0 for key in KEYLIST_DICT["up"]):
            return (probe_stim.ori) % 360, ori_keylog

        # rotate counter-clockwise
        if any(key_down[key] > 0 for key in KEYLIST_DICT["left"]):
            dirsign = -acceleration_per_frame(
                np.sum(key_down[key] for key in KEYLIST_DICT["left"])
            )
        # rotate clockwise
        elif any(key_down[key] > 0 for key in KEYLIST_DICT["right"]):
            dirsign = acceleration_per_frame(
                np.sum(key_down[key] for key in KEYLIST_DICT["right"])
            )
        # do not rotate
        else:
            dirsign = 0

        # change the image orientation
        probe_stim.ori += ROT_ACCELERATION * dirsign

        # draw probe stimulus each frame
        probe_stim.draw()
        win.flip()

    # if response window timed out, send trigger
    trigger_kwargs = trigger_kwargs_list[-1]
    send_trigger(**trigger_kwargs)

    return (probe_stim.ori) % 360, ori_keylog


def display_control_wedge(
    win, kb, ctrl_width, ctrl_ori, target_width, target_ori, trigger_kwargs_list
):
    """Displays the confidence rating as a circle wedge with adjustable angular size. For this we
    only allow the keys from the KEYLIST_DICT as inputs. For each frame we check if a key has been
    pressed and released and count each consecutive frame to adjust the acceleration of the item
    rotation. For this we take the sum over all keys that do the same, e.g. 'a' and 'left', so that
    both left and right-handed participants can easily do the task.

    Parameters
    ----------
    win : psychopy.visual.Window
        the window in which we display the rating scale
    kb : psychopy.hardware.keyboard
        the keyboard object from which we get the key presses
    scale : array-like
        start and end values of the rating
    ctrl_width : float
        the initial width of the control wedge. The value is assumed to be between 0 and 1
    ctrl_ori : float
       the initial orientation of the control wedge
    target_width : float
        the width of the target wedge displayed at the top of the screen
    target_ori : float
        the orientation of the target wedge displayed at the top of the screen
    trigger_kwargs_list : list of dicts
        Each entry in the list corresponds to a certain action flagged with a trigger.

    Returns
    -------
    task_rating : float
        the final angle of the wedge mapped between 0 and 1, where 1 is the smallest
    wedge_angle : float
        the final angle of the wedge in degrees
    task_keylog : list
        the list of key presses
    """
    # initialize the wedge object
    trigger_kwargs = trigger_kwargs_list[0]
    win.callOnFlip(send_trigger, **trigger_kwargs)
    wedge = get_confidence_wedge(win, ctrl_width, ctrl_ori)

    # initialize target wedge on top
    target_wedge = get_ctrl_target_wedge(win, target_width, target_ori)

    # get list of allowed keys
    key_list = [key for action_list in KEYLIST_DICT.values() for key in action_list]

    # keep a tap of which keys are pressed down
    key_down = {key: 0 for key in key_list}
    task_keylog = []

    # let user change probe orientation with key presses
    for _ in range(WAIT_RESPONSE_PROBE_FRAMES):
        # reset movement
        dirsign = 0

        # get, but do not clear, pressed keys
        keys = kb.getKeys(key_list, waitRelease=False, clear=False)
        # get and clear key when released
        keys_released = kb.getKeys(key_list, waitRelease=True, clear=True)

        # print pressed and released keys to command line
        if keys:
            # count frames where key is pressed down, used to adapt item acceleration
            for k in keys:
                key_down[k.name] += 1
                # append single key press to key log
                if key_down[k.name] == 1:
                    task_keylog.append(["pressed: ", [(k.rt, k.name) for k in keys]])
        if keys_released:
            # reset key frame count
            for k in keys:
                key_down[k.name] = 0
                # append single key press to key log
                if key_down[k.name] != 0:
                    task_keylog.append(
                        ["released: ", [(k.rt, k.name) for k in keys_released]]
                    )

        # if the pressed key is enter, we can end the loop
        if any(key_down[key] > 0 for key in KEYLIST_DICT["up"]):
            wedge_diff = np.abs(wedge.end - wedge.start)
            task_rating = 1 - wedge_diff / 360
            return task_rating, wedge_diff, task_keylog

        # rotate counter-clockwise
        if any(key_down[key] > 0 for key in KEYLIST_DICT["left"]):
            dirsign = -acceleration_per_frame(
                np.sum(key_down[key] for key in KEYLIST_DICT["left"])
            )
        # rotate clockwise
        elif any(key_down[key] > 0 for key in KEYLIST_DICT["right"]):
            dirsign = acceleration_per_frame(
                np.sum(key_down[key] for key in KEYLIST_DICT["right"])
            )
        # do not shift
        else:
            dirsign = 0

        # change the cursor position
        ws = wedge.start - ROT_ACCELERATION * dirsign
        we = wedge.end + ROT_ACCELERATION * dirsign
        # only update if the new values do not exceed a full circle
        if -360 < (we - ws) < 360:
            wedge.start = ws
            wedge.end = we

        # display slider
        wedge.draw()
        target_wedge.draw()

        # update screen
        win.flip()

    # if response window timed out, send trigger
    trigger_kwargs = trigger_kwargs_list[-1]
    send_trigger(**trigger_kwargs)

    wedge_diff = np.abs(wedge.end - wedge.start)
    task_rating = 1 - wedge_diff / 360
    return task_rating, wedge_diff, task_keylog


def display_confidence_probe(
    win, kb, image_data, conf_width, conf_ori, trigger_kwargs_list
):
    """Displays the probe image transparent and overlayed two times with a wedge
    that participants can control to adjust their confidence. For this we
    only allow the keys from the KEYLIST_DICT as inputs. For each frame we check if a key has been
    pressed and released and count each consecutive frame to adjust the acceleration of the item
    rotation. For this we take the sum over all keys that do the same, e.g. 'a' and 'left', so that
    both left and right-handed participants can easily do the task.

    Parameters
    ----------
    win : psychopy.visual.Window
        the window in which we display the rating scale
    kb : psychopy.hardware.keyboard
        the keyboard object from which we get the key presses
    image_data : array-like
        the probe image as a numpy array
    conf_width : float
        the initial position of the cursor. The value is assumed to be between 0 and 1
    conf_ori : float
        the center of the wedge around which the user can adjust the size
    trigger_kwargs_list : list of dicts
        Each entry in the list corresponds to a certain action flagged with a trigger.

    Returns
    -------
    task_rating : float
        the final angle of the wedge mapped between 0 and 1, where 1 is the smallest
    wedge_angle : float
        the final angle of the wedge in degrees
    task_keylog : list
        the list of key presses
    """
    # create probe image again
    probe_cw = get_image_stim(
        win,
        image_data,
        conf_ori,
        size=IMAGE_SIZE_DVA,
        opacity=0.3,
    )[0]

    probe_ccw = get_image_stim(
        win,
        image_data,
        conf_ori,
        size=IMAGE_SIZE_DVA,
        opacity=0.3,
    )[0]

    # initialize the wedge object
    trigger_kwargs = trigger_kwargs_list[0]
    win.callOnFlip(send_trigger, **trigger_kwargs)
    wedge = get_confidence_wedge(
        win,
        conf_width,
        conf_ori,
        color=(0.6, 0.6, 0.6),
        opacity=0.5,
        linecolor=(0.4, 0.4, 0.4),
    )

    # get list of allowed keys
    key_list = [key for action_list in KEYLIST_DICT.values() for key in action_list]

    # keep a tap of which keys are pressed down
    key_down = {key: 0 for key in key_list}
    task_keylog = []

    # let user change probe orientation with key presses
    for _ in range(WAIT_RESPONSE_PROBE_FRAMES):
        # reset movement
        dirsign = 0

        # get, but do not clear, pressed keys
        keys = kb.getKeys(key_list, waitRelease=False, clear=False)
        # get and clear key when released
        keys_released = kb.getKeys(key_list, waitRelease=True, clear=True)

        # check for pressed and released keys
        if keys:
            # count frames where key is pressed down
            for k in keys:
                key_down[k.name] += 1
                # append single key press to key log
                if key_down[k.name] == 1:
                    task_keylog.append(["pressed: ", [(k.rt, k.name) for k in keys]])
        if keys_released:
            # reset key frame count
            for k in keys:
                key_down[k.name] = 0
                # append single key press to key log
                if key_down[k.name] != 0:
                    task_keylog.append(
                        ["released: ", [(k.rt, k.name) for k in keys_released]]
                    )

        # if the pressed key is enter, we can end the loop
        if any(key_down[key] > 0 for key in KEYLIST_DICT["up"]):
            wedge_diff = np.abs(wedge.end - wedge.start)
            task_rating = 1 - wedge_diff / 360
            return task_rating, wedge_diff, task_keylog

        # rotate counter-clockwise
        if any(key_down[key] > 0 for key in KEYLIST_DICT["left"]):
            dirsign = -acceleration_per_frame(
                np.sum(key_down[key] for key in KEYLIST_DICT["left"])
            )
        # rotate clockwise
        elif any(key_down[key] > 0 for key in KEYLIST_DICT["right"]):
            dirsign = acceleration_per_frame(
                np.sum(key_down[key] for key in KEYLIST_DICT["right"])
            )
        # do not rotate
        else:
            dirsign = 0

        # change the cursor position
        ws = wedge.start - ROT_ACCELERATION * dirsign
        we = wedge.end + ROT_ACCELERATION * dirsign
        # only update if the new values do not exceed a full circle
        if -360 < (we - ws) < 360:
            wedge.start = ws
            wedge.end = we
            # change the image orientation
            probe_cw.ori += ROT_ACCELERATION * dirsign
            probe_ccw.ori -= ROT_ACCELERATION * dirsign

        # draw probe stimulus each frame
        wedge.draw()
        probe_cw.draw()
        probe_ccw.draw()
        win.flip()

    # if response window timed out, send trigger
    trigger_kwargs = trigger_kwargs_list[-1]
    send_trigger(**trigger_kwargs)

    wedge_diff = np.abs(wedge.end - wedge.start)
    task_rating = 1 - wedge_diff / 360
    return task_rating, wedge_diff, task_keylog


def acceleration_per_frame(i_frame):
    """Returns the acceleration as a function of the frame the object is rotated.
    we build a stepwise function that increases acceleration linearly and then
    continues constantly.

    The acceleration is given between 0 and 1 and can be multiplied with any factor
    in the display functions
    """
    return np.minimum(1, i_frame / (2 * EXPECTED_FPS))


def display_instructions(win, kb, task_switch=None):
    """Display participant instructions.

    Parameters
    ----------
    win : psychopy.visual.Window
        The psychopy window on which to draw the stimuli.
    kb : psychopy.hardware.keyboard
        The psychopy keyboard object.
    task_switch : str, None | None (default)
        if only either of the task instructions should be displayed. Can be
        'confidence' or 'control' for either task. If 'None' displays the
        whole instruction.
    """
    text_stim = get_central_text_stim(win=win, height=TEXT_HEIGHT_DVA)

    # prepare instructions
    instructions_start = [
        "Welcome \n\n\n"
        "Use the left and right arrow keys to navigate through the instructions.",
        "In this study, we want to investigate how humans judge their own performance "
        "in a short-term memory task.",
        "In each trial we will show you a sequence of 1 to 3 objects. \n"
        "All of them are rotated in a specific angle. \n",
        "In each trial, you are tasked to report the orientation of one of the shown objects.",
    ]
    instructions_ori_task = [
        "To report the orientation of the shown item, you need to memorize all presented objects as well as you can. "
        "After a brief pause, you will be presented with one of them at random "
        "and your task is to rotate the object to your memorized orientation. \n\n",
        "For this you can use the left and right arrow keys or 'a' and 'd'. \n\n "
        "When you are satisfied, use the up key or 'w' to confirm. ",
        "Within each trial and after the first orientation task, you will also be asked to either judge your "
        "performance or replicate a figure.",
    ]
    instructions_conf_task = [
        "In the Performance Judgment task, you will be presented with the same object "
        "again and your task is to adjust the width of an error wedge - indicated by same object - such that you are "
        "Certain that the object orientation lies within the wedge. \n\n",
        "You get a higher reward the smaller the error wedge is, but if the true object orientation is "
        "outside your indicated range, you will get NO reward.",
    ]
    instructions_ctrl_task = [
        "In the Visual Reproduction task, you will replicate the size of a wedge that is presented "
        "on top of the screen.\n",
        "For this you can adjust the width of the center wedge to match "
        "the target on top as closely as possible. \n",
        "You will get a higher reward the closer your wedge is to the target above, but if it "
        "is too small, you will get NO reward.",
    ]
    instructions_end = [
        "This sounds complicated, but it is really easy and we will show a few practice trials before we begin.",
        f"For each task you have a maximum of {max_wait_response} seconds to answer. "
        "If you do not answer in time (using the 'w' or up keys), the experiment will proceed to the next trial "
        "and count your last input as your response.",
        "In all trials you will report the object orientation and in one half do the Performance Judgment "
        "task and in the other half the Visual Reproduction task.",
        "The task will only change once and which of the two tasks will come first will be decided at random. \n\n"
        "We will let you know what to do and when the task changes!",
        f"Every {N_FEEDBACK_BLOCK:d} trials you will receive feedback about how "
        "accurate your choices were during those trials. ",
        f"The whole experiment has a total of {NTRIALS:d} trials. \n\n"
        "Remember that over the whole experiment you can earn a bonus of "
        f"up to {MAX_REWARD} Euros, depending on your performance.\n\n"
        "-> Pressing the right key will end the instructions. <-",
    ]

    instructions_switch = [
        "Now we will proceed with the following task: \n",
        "You will be presented with a series of objects and are tasked to rotate one of "
        "them to it's original orientation. \n",
        "Then, ... ",
    ]

    # select text to display
    instructions = (
        instructions_start
        + instructions_ori_task
        + instructions_conf_task
        + instructions_ctrl_task
        + instructions_end
    )
    # select only instructions when switching tasks between blocks
    if task_switch == "control":
        instructions = instructions_switch + instructions_ctrl_task
    elif task_switch == "confidence":
        instructions = instructions_switch + instructions_conf_task

    # Instructions presentation
    itext = 0
    key_list = [key for action_list in KEYLIST_DICT.values() for key in action_list]
    while True:
        text_stim.text = instructions[itext]
        text_stim.draw()
        win.flip()
        keys = kb.waitKeys(keyList=key_list)
        if keys[0] in KEYLIST_DICT["quit"]:
            break
        elif keys[0] in KEYLIST_DICT["left"]:
            # can't go further back than 0
            itext = max(0, itext - 1)
        elif keys[0] in KEYLIST_DICT["right"]:
            itext += 1
            if itext >= len(instructions):
                break


def display_survey_gui():
    """Gather participant and experiment data.

    Creates a directory and writes data to it.

    Returns
    -------
    run_type : {"instructions", "training", "experiment", "testing"}
        The type/mode that the experiment runs in. "experiment" should be
        used to run participants, "training" saves data to "Test" subject
        folders (always overwritten), shows fewer trials, and displays
        additional feedback. "instructions" will just display participant
        instructions and then quit. "bonus" will just display earned
        bonus money and then quit.
    subjdir : pathlib.Path | None
        Path object pointing to the directory where to save data for this
        participant. Will be None if `run_type` is "instructions".
    subtype : str
        either 'sub' or 'pil' depending on whether the subject is for piloting or
        final data recording
    substr : str | None
        The subject identifier string, either of format f"{int:02}", or "test".
    subid : int
        the subject id as an integer
    """
    # Check for real experiment or just a test run
    survey_gui1 = gui.Dlg(title="Confidence Experiment")
    survey_gui1.addField(
        "Type",
        choices=[
            "instructions",
            "training",
            "experiment",
            "Calculate bonus",
            "_testing",
        ],
    )
    survey_data1 = survey_gui1.show()

    # Prepare directory for saving data
    conf_dir = Path(__file__).resolve().parent.parent
    data_dir = conf_dir / "experiment_data"
    msg = f"Sure you are in the right directory? {conf_dir}, {data_dir}"
    assert data_dir.exists(), msg

    if not survey_gui1.OK:
        # Cancel the program in case of "cancel"
        core.quit()

    run_type = survey_data1[0]

    if run_type == "instructions":
        return run_type, None, None, None, None

    elif run_type == "experiment":
        # We want to run the experiment, gather some data
        survey_gui2 = gui.Dlg(title="Confidence Experiment")
        survey_gui2.addField("Study Type", choices=["Subject", "Pilot"])
        survey_gui2.addField("ID:", choices=list(range(1, 100)))
        survey_gui2.addField("Age:", choices=list(range(18, 60)))
        survey_gui2.addField("Sex:", choices=["Male", "Female", "Other"])
        survey_gui2.addField("Handedness:", choices=["Right", "Left", "Ambidextrous"])
        survey_data2 = survey_gui2.show()

        if not survey_gui2.OK:
            # Cancel the program in case of "cancel"
            core.quit()

    elif run_type == "training":
        # for training we only need the participant ID
        survey_gui2 = gui.Dlg(title="Confidence Experiment")
        survey_gui2.addField("Study Type", choices=["Subject", "Pilot"])
        survey_gui2.addField("ID:", choices=list(range(1, 100)))
        survey_data2 = survey_gui2.show()

        if not survey_gui2.OK:
            # Cancel the program in case of "cancel"
            core.quit()

    elif run_type == "Calculate bonus":
        # After collecting data we can choose this option to calculate their bonus (again)
        survey_gui2 = gui.Dlg(title="Confidence Experiment")
        survey_gui2.addField("Study Type", choices=["Subject", "Pilot"])
        survey_gui2.addField("ID:", choices=list(range(1, 100)))
        survey_data2 = survey_gui2.show()

    else:
        assert run_type == "_testing"
        survey_data2 = ["sub", "test"]

    # Create subj dir
    subtype = survey_data2[0]
    if subtype == "Pilot":
        subtype = "pil"
    else:
        subtype = "sub"
    subid = survey_data2[1]
    substr = f"{subid:02}" if isinstance(subid, int) else subid
    subjdir = data_dir / f"{subtype}-{substr}"

    # create directories for subject
    os.makedirs(subjdir, exist_ok=True)

    # Save available participant data
    recording_datetime = datetime.datetime.today().isoformat()
    if len(survey_data2) > 1:
        kwargs = dict(zip(["Study Type", "ID", "Age", "Sex", "Handedness"], survey_data2))
    else:
        kwargs = dict(zip(["Study Type", "ID"], survey_data2))
    data = dict(
        # experiment_version=experiment.__version__,
        recording_datetime=recording_datetime,
    )
    data.update(kwargs)

    if run_type == "training":
        logfile_ext = "_training"
    else:
        logfile_ext = ""
    fname = f"{subtype}-{substr}_info{logfile_ext}.json"
    fpath = subjdir / fname
    if fpath.exists() and run_type == "experiment":
        raise RuntimeError(f"File exists: {fpath}")
    with open(fpath, "w", encoding="utf-8") as fout:
        json.dump(data, fout, indent=4, ensure_ascii=False, sort_keys=True)

    return run_type, subjdir, subtype, substr, subid


def display_block_break(
    win,
    kb,
    logfile,
    itrial,
    ntrials,
    blocksize,
    block_counter,
    hard_break,
    task_name,
    next_task_name,
    eyetrack_dict,
    trigger_kwargs,
):
    """Display a break screen, including feedback.
    We utilize this function to show feedback at the end of every trial in the practice run and
    show the feedback only at the end of every block in the main experiment.
    For the practice run this is achieved by setting the blocksize in the practice run to 1 and
    the hard_break to be larger than the number of trials in the whole practice.
    For the main experiment we set the blocksize to the number of trials to do a hard-break, so
    only when there is a block break do participants receive feedback.

    Parameters
    ----------
    win : psychopy.visual.Window
        The psychopy window on which to draw the stimuli.
    kb : psychopy.hardware.keyboard
        the keyboard object from which we get the key presses
    logfile : pathlib.Path
        Path object pointing to the logfile for this stream.
    itrial : int
        The current trial number
    ntrials : int
        The overall number of trials.
    blocksize : int
        How many trials fit into one block.
    block_counter : int
        A simple counter variable, that will be incremented by 1 and then returned.
    hard_break : int
        Used to determine whether to do a non-skippable block break. Done via
        ``block_counter % hard_break == 0``, that is, do a non-skippable block after
        every `hard_break` blocks. Hard breaks can only be skipped by pressing
        the escape key (see KEYLIST_DICT in define_settings.py).
    eyetrack_dict : dict
        Dictionary containing the eye-tracker object and EDF file names.
    trigger_kwargs : dict
        Contains keys ser, tk, and byte. To be passed to the send_trigger
        function.

    Returns
    -------
    block_counter : int
        A simple block counter, incremented by one compared to how it was passed
        into this function.
    """
    # Setup serial port
    ttl_dict = get_ttl_dict()

    # every some blocks we do a hard break where we wait for the experimenter
    do_hard_break = block_counter % hard_break == 0

    # check if we need to show instructions for the next task i.e., if we switch blocks
    show_instructions = task_name != next_task_name

    # compute the accuracy of the last block
    final_score = calculate_performance(logfile, blocksize)
    monetary_reward = np.round(np.sum(final_score * REWARD_CONSTANT), decimals=3)
    maximum_reward = np.round(blocksize * REWARD_CONSTANT, decimals=2)

    text = f"You have completed {itrial+1} of {ntrials} trials.\n\n"
    text += f"In the past {blocksize} trials "
    text += (
        f"you earned an overall {monetary_reward} out of a maximum of {maximum_reward} "
        "Euros in this task so far.\n\n"
    )

    # change text depending on what break
    if do_hard_break:
        text += "\n>> Please wait for the experimenter. <<"
        text_stim = get_exp_text_stim(win, TEXT_HEIGHT_DVA, text)
    else:
        text += "Press any key to continue."
        text_stim = get_central_text_stim(win, TEXT_HEIGHT_DVA, text)

    text_stim.draw()
    # send trigger for block break
    win.callOnFlip(send_trigger, **trigger_kwargs)
    win.flip()

    # quit block break if it was the last trial
    if (1 + itrial) == ntrials:
        return

    # don't record break / re-calibration
    stop_eye_recording(eyetrack_dict["tk"])
    if do_hard_break:
        # only pressing escape works (see KEYLIST_DICT in define_settings.py)
        kb.waitKeys(keyList=KEYLIST_DICT["quit"])

        # do eye tracking drift check
        if not isinstance(eyetrack_dict["tk"], DummyEyeLink):
            # do a drift correction (i.e., a check and potential re-calibration) here
            # Enter = confirm drift check and go on
            # ESC = enter re-calibration
            #     in re-calibration:
            #         c = calibrate
            #         v = validate
            #         Enter = confirm
            #         ESC = exit re-calibration
            # after potential re-calibration, a drift check is started again
            # ... until a successful drift check (=confirmed through Enter)
            check_drift(eyetrack_dict["tk"], win)

            # then, let experimenter get ready again (only escape works)
            text_stim.text = ">> Please wait. <<"
            text_stim.draw()
            win.flip()
            kb.waitKeys(keyList=KEYLIST_DICT["quit"])
    else:
        # self paced wait for participant
        kb.waitKeys()

    # present instructions for the change in tasks if the next is different
    if show_instructions:
        # display changing task instructions
        display_instructions(win, kb, task_switch=next_task_name)

    # then, a participant can start as they want
    text_stim.text = "Press any key to continue."
    text_stim.draw()
    win.flip()

    # self paced wait for participant
    kb.waitKeys()

    # wait so trigger can pass without iti interfering
    trigger_kwargs["value"] = ttl_dict["offset_block_break"]
    send_trigger(**trigger_kwargs)
    core.wait(secs=0.005)

    # restart recording
    start_eye_recording(eyetrack_dict["tk"])

    return block_counter + 1


def calculate_monetary_bonus(subjdir, subtype, substr):
    """Calculate the monetary bonus at the end of the experiment and print
    it to the command line

    Parameters
    ----------
    subjdir : pathlib.Path
        directory of the experiment behavioral output files
    subtype : str
        type of participant. Either is `pil` or `sub`.
    substr : str
        a string indicating the subject ID
    """
    # load the non-trianing behavioral file
    logfile = subjdir / f"{subtype}-{substr}_beh.tsv"
    # check that the file actually exists
    if not os.path.isfile(logfile):
        print("     >>> Behavioral file for this subject cannot be found <<<")
        print(f"{subjdir}")
        print(f"{subtype}")
        print(f"{substr}")
        print("     >>> Are you sure the data exists? <<<")

    # calculate performance and round bonus to full Euros
    final_score = calculate_performance(logfile, blocksize=NTRIALS)
    monetary_reward = np.round(np.sum(final_score * REWARD_CONSTANT), decimals=1)
    maximum_reward = NTRIALS * REWARD_CONSTANT

    # print bonus to command line
    print("\n")
    print(f"Participant {subtype}-{substr} earned an extra of")
    print(f"    {monetary_reward} out of a maximum of {maximum_reward} Euros")
    print("Congratulations!")


def cleanup_experiment(win, kb, ser_port):
    """Wait for button press to end the experiment and clean up psychopy objects.

    Parameters
    ----------
    win : psychopy.visual.Window
        The psychopy window on which to draw the stimuli.
    kb : psychopy.hardware.keyboard
        the keyboard object from which we get the key presses
    ser_port : serial port
        the trigger port
    """
    # TODO:
    # print the performance for all trials at the end

    # define text to show and the end
    text = "You have successfully completed the whole experiment \n\n"
    text = text + "Press Q to quit"
    # create a text stimulus
    text_stim = get_central_text_stim(
        win,
        height=TEXT_HEIGHT_DVA,
        text=text,
    )

    # print text to screen and wait for button press
    text_stim.draw()
    win.flip()
    kb.waitKeys(keyList=KEYLIST_DICT["quit"])

    # close serial port
    ser_port.ser.close()

    # close screen
    win.close()
