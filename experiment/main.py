"""Main flow of the conf_primer experiment.

TODO:
 - double check that the fixation dot has the correct size i.e., 8x8 pixels (see. Juan's paper)
 - images in Juan's project were presented at about 6.5° visual angle
     psychopy can take visual angle in degrees as an input parameter for displaying stimuli. For
     this it converts the angle to pixels using the setDistance in the monitors object. If that
     distance is correct, it should all be working fine.
 - Do we want to use stimuli that are uright, square and wide or do we want to use images that are
   only upright and wide?
"""

# %%
import datetime

import numpy as np
from define_eyetracking import (
    calibrate_eyetracker,
    check_drift,
    convert_edf2asc,
    exit_eye_recording,
    setup_eyetracker,
    start_eye_recording,
    update_status,
)
from define_routines import (
    calculate_monetary_bonus,
    cleanup_experiment,
    display_block_break,
    display_confidence_probe,
    display_control_wedge,
    display_instructions,
    display_iti,
    display_probe_response,
    display_survey_gui,
    display_trial_images,
)
from define_settings import (
    BLOCKSIZE,
    CALIBRATION_TYPE,
    EXPECTED_FPS,
    FIXSTIM_OFF_FRAMES,
    FULLSCR,
    IMAGE_FRAMES,
    IMAGE_SIZE_DVA,
    ISI_FRAMES,
    KEYLIST_DICT,
    MAINTENANCE_FRAMES,
    MAX_ITI_MS,
    MIN_ITI_MS,
    MONITOR_NAME,
    N_FEEDBACK_BLOCK,
    NBLOCKS,
    NIMAGES,
    NREPEATS_PER_ORI,
    NTRIALS,
    NTRIALS_PER_COND,
    SER_ADDRESS,
    SER_WAIT_MICROSECS,
    TEXT_HEIGHT_DVA,
    TK_DUMMY_MODE,
    checkTiming,
    kb_backend,
    n_hard_break,
)
from define_stimuli import (
    get_exp_text_stim,
    get_fixation_stim,
    get_image_stim,
    load_images,
)
from define_trials import (
    gen_all_balanced_trials,
    subselect_trials,
)
from define_ttl import (
    FakeSerial,
    MySerial,
    get_ttl_dict,
    send_trigger,
)
from psychopy import core, monitors, visual
from psychopy.hardware import keyboard
from utils import (
    check_framerate,
    circ_dist,
    save_dict,
)

# %%

# prepare logging, where we save data from the participants
run_type, subjdir, subtype, substr, subid = display_survey_gui()


# %%
# define graphics variables and initialize hardware

# Prepare monitor
my_monitor = monitors.Monitor(name=MONITOR_NAME)

# prepare the window
width, height = my_monitor.getSizePix()

print(width, height)

win = visual.Window(
    color=(0.8, 0.8, 0.8),
    fullscr=FULLSCR,
    monitor=my_monitor,
    units="deg",
    winType="pyglet",
    size=(width, height),
)
win.mouseVisible = False

print(f"screen size: {width} by {height}")
print(f"window size: {win.size}")

# initialize screen framerate
if checkTiming:
    fps = check_framerate(win, EXPECTED_FPS)
else:
    fps = EXPECTED_FPS


# init the keyboard, backend either 'event' or 'ptb'
kb = keyboard.Keyboard(backend=kb_backend)

print("Using keyboard backend: ", kb._backend)


# *if just instructions*, display and quit.
if run_type == "instructions":
    display_instructions(win, kb)
    win.close()
    core.quit()

# ntrials - total number of trials
# n_hard_break - number of trials per hard block
# BLOCKSIZE - number of trials per self-paced block with feedback at the end


# generate trials
trlgen_seed = None
trials = gen_all_balanced_trials(
    subid=subid,
    n_images=NIMAGES,
    nrepeats_per_ori=NREPEATS_PER_ORI,
    BLOCKSIZE=BLOCKSIZE,
    NBLOCKS=NBLOCKS,
    NTRIALS_PER_COND=NTRIALS_PER_COND,
    NTRIALS=NTRIALS,
)

# default logfile extension to distinguish training from non training files
logfile_ext = ""


# run the training with feedback after each trial
if run_type == "training":
    # define trial numbers
    ntrials = 20
    N_FEEDBACK_BLOCK = 1
    n_hard_break = 10
    BLOCKSIZE = 10
    logfile_ext = "_training"
    # select some trials per task, first and last ntrials
    train_trial_index = np.r_[: int(ntrials / 2), -int(ntrials / 2) : 0]
    trials = {key: val[train_trial_index, ...] for key, val in trials.items()}

# run the full experiment
elif run_type == "experiment":
    ntrials = NTRIALS

    # create a shorter main experiment for pilot subject
    if subtype == "pil":
        # TODO:
        # if we redo the trial generation to have the same number of orientations
        # per load condition, we only need to set the nrepeats_per_ori to half the
        # number (i.e., 2) to get half the trials
        # Still make sure that block size and breaks are changed accordingly
        ntrials = int(NTRIALS / 2)
        NTRIALS_PER_COND = int(NTRIALS_PER_COND / 2)
        BLOCKSIZE = int(BLOCKSIZE / 2)
        N_FEEDBACK_BLOCK = int(N_FEEDBACK_BLOCK / 2)
        NREPEATS_PER_ORI = int(NREPEATS_PER_ORI / 2)

        # select some trials per task, first and last ntrials
        # train_trial_index = np.r_[:int(ntrials/2), -int(ntrials/2):0]
        # trials = {key : val[train_trial_index, ...] for key, val in trials.items()}

        trials = subselect_trials(trials, ntrials)

# calculate the bonus after data collection
elif run_type == "Calculate bonus":
    # loads the non-training behavioral file and returns bonus to command line
    calculate_monetary_bonus(subjdir, subtype, substr)
    win.close()
    core.quit()

# test paradigm for development - not to be used with participants
elif run_type == "_testing":
    ntrials = 4
    N_FEEDBACK_BLOCK = 2
    n_hard_break = 2
    BLOCKSIZE = 2
    NTRIALS_PER_COND = 1

    # select some trials per task, first and last ntrials
    # train_trial_index = np.r_[:int(ntrials/2), -int(ntrials/2):0]
    # print(train_trial_index)
    # print([type(val) for key, val in trials.items()])
    # trials = {key : val[train_trial_index, ...] for key, val in trials.items()}

    trials = subselect_trials(trials, ntrials)
    print(trials)


# Prepare logfile, edf name must be a string and not a Path
if substr is not None:
    seqfile = subjdir / f"{subtype}-{substr}_sequence{logfile_ext}.tsv"
    logfile = subjdir / f"{subtype}-{substr}_beh{logfile_ext}.tsv"
    keyfile = subjdir / f"{subtype}-{substr}_key{logfile_ext}.tsv"
    edf_fname_local = str(subjdir / f"{subtype}-{substr}_eyetrack.edf")

print(logfile)
# save generated trials
save_dict(seqfile, trials)


# %%
# define visual stimuli

# fixation cross/dot
fixation_stim_parts = get_fixation_stim(win, stim_type="dot")

# preload all images from file
image_data, image_set = load_images(subid)


# setup random number generators and clock
rt_clock = core.Clock()
iti_rng = np.random.default_rng()
state_rng = np.random.default_rng()
# start counting blocks at 1
block_counter = 1


# set up eye tracking
# (only track eyes in "experiment" mode and if TK_DUMMY_MODE is False)
month_day_hour_minute = datetime.datetime.today().strftime("%m%d%H%M")
edf_fname = f"{month_day_hour_minute}.edf"
tk_dummy_mode = TK_DUMMY_MODE if run_type == "experiment" else True
tk = setup_eyetracker(tk_dummy_mode, my_monitor, edf_fname, CALIBRATION_TYPE)
tk = calibrate_eyetracker(tk, win)
# create eyetracking dict
eyetrack_dict = dict(tk=tk, edf_fname=edf_fname, edf_fname_local=edf_fname_local)
# do another drift check to assert that calibration was done nicely
check_drift(eyetrack_dict["tk"], win)

# Start eye-tracking
error = start_eye_recording(eyetrack_dict["tk"])
assert error == 0, "Problem during eye-tracker setup."


# Setup serial port
ttl_dict = get_ttl_dict()

if SER_ADDRESS is None:
    ser_port = MySerial(FakeSerial(), wait_microsecs=SER_WAIT_MICROSECS)
    print("No serial port specified. We will not send TTL triggers to host PC.")
else:
    ser_port = MySerial(SER_ADDRESS, wait_microsecs=SER_WAIT_MICROSECS)

core.wait(1)

trigger_kwargs = dict(ser=ser_port, tk=tk, value=0)
send_trigger(**trigger_kwargs)


# =============================================================================
# start experiment
# =============================================================================
# Send reminder: EEG recording?
if run_type == "experiment":
    reminder_stim = get_exp_text_stim(
        win,
        height=TEXT_HEIGHT_DVA,
        text=">> Wait for Experimenter to\n\nstart EEG recording. <<",
    )
    reminder_stim.draw()
    win.flip()
    kb.waitKeys(keyList=KEYLIST_DICT["quit"])


start_stim = get_exp_text_stim(
    win,
    height=TEXT_HEIGHT_DVA,
    text=">> Please wait for the experimenter. <<",
)
start_stim.draw()
win.flip()
kb.waitKeys(keyList=KEYLIST_DICT["quit"])

# display instructions for the first task to get participant going
display_instructions(win, kb, task_switch=trials["block_sequence"][0])

# let participants start when they read the instructions
start_stim.text = "Press any key to start."
start_stim.draw()
win.flip()
kb.waitKeys()

# get start time
t_start_exp = rt_clock.getTime()

# Show fixstim
for stim in fixation_stim_parts:
    stim.setAutoDraw(True)
win.flip()

# send start experiment trigger
trigger_kwargs["value"] = ttl_dict["begin_experiment"]
send_trigger(**trigger_kwargs)
core.wait(1)


# =============================================================================
# run trials
# =============================================================================
for itrial in range(ntrials):
    # get state for this trial
    state = state_rng.choice([0, 1])

    # update current trial information in eye tracking screen
    update_status(
        tk=eyetrack_dict["tk"],
        block=block_counter,
        max_block=int(ntrials / N_FEEDBACK_BLOCK),
        trial=1 + (itrial % N_FEEDBACK_BLOCK),
        max_trial=N_FEEDBACK_BLOCK,
    )

    # get task name of current trial
    task_name = trials["block_sequence"][itrial]
    # get task name of the next trial, if there is any
    if itrial < ntrials - 1:
        next_task_name = trials["block_sequence"][itrial + 1]
    else:
        next_task_name = task_name

    # =============================================================================
    # pre trial ITI
    # =============================================================================

    # Show fixstim
    for stim in fixation_stim_parts:
        stim.setAutoDraw(True)

    # jittered inter-trial-interval
    trigger_kwargs["value"] = ttl_dict["new_trl"]
    iti_ms = display_iti(win, MIN_ITI_MS, MAX_ITI_MS, fps, iti_rng, trigger_kwargs)

    # get time stamp
    t_onset_trial = rt_clock.getTime()

    # 500 ms before first sample onset, remove fixstim
    for stim in fixation_stim_parts:
        stim.setAutoDraw(False)

    trigger_kwargs["value"] = ttl_dict["fixstim_offset"]
    win.callOnFlip(send_trigger, **trigger_kwargs)
    for frame in range(FIXSTIM_OFF_FRAMES):
        win.flip()

    # =============================================================================
    # display stimuli consecutively
    # =============================================================================

    # trial image indices
    n_items = trials["load_sequence"][itrial].astype(int)
    image_indices = trials["img_id"][itrial, :n_items].astype(int)
    image_orientations = trials["img_ori"][itrial, :n_items]

    print("Stimulus orientation")
    print(image_orientations)

    # get time stamp
    t_onset_items = rt_clock.getTime()

    # prepare the psychopy images for this trial
    image_stims = get_image_stim(
        win,
        image_data[image_indices],
        image_orientations,
        size=IMAGE_SIZE_DVA,
    )

    # prepare trigger list
    trigger_kwargs_list = [
        dict(ser=ser_port, tk=tk, value=ttl_dict[f"seq_{i_seq}_item_{i_image}"])
        for i_seq, i_image in enumerate(image_indices)
    ]

    # display stimuli
    display_trial_images(
        win,
        image_frames=IMAGE_FRAMES,
        isi_frames=ISI_FRAMES,
        image_stims=image_stims,
        trigger_kwargs_list=trigger_kwargs_list,
    )

    # =============================================================================
    # display maintenance period with fixation cross only
    # =============================================================================

    # get time stamp
    t_onset_delay = rt_clock.getTime()
    # get trigger for maintenance interval
    trigger_kwargs["value"] = ttl_dict["maintenance"]

    for stim in fixation_stim_parts:
        stim.setAutoDraw(True)

    win.callOnFlip(send_trigger, **trigger_kwargs)
    for _ in range(MAINTENANCE_FRAMES):
        win.flip()

    # 500 ms before probe, remove fixation again
    for stim in fixation_stim_parts:
        stim.setAutoDraw(False)

    for frame in range(FIXSTIM_OFF_FRAMES):
        win.flip()

    # =============================================================================
    # probe stimulus
    # =============================================================================

    # present one of the items at random and let participant change the orientation
    probe_pos = trials["probe_pos"][itrial].astype(int)
    # get image index from nth image presented
    probe_image_ind = trials["probe_id"][itrial].astype(int)
    # get orientation of probe at start of test
    probe_ori_start = trials["probe_ori_start"][itrial]
    # get the orientation of the probe as it was actually presented
    probe_ori = trials["probe_ori"][itrial]

    # prepare trigger list
    response_trigger_actions = ["response_prompt", "first_key_down", "response_timeout"]

    trigger_kwargs_list = [
        dict(ser=ser_port, tk=tk, value=ttl_dict[f"{action}"])
        for action in response_trigger_actions
    ]

    # get time stamp
    t_onset_probe = rt_clock.getTime()

    # display probe and let participants respond
    final_ori, probe_keylog = display_probe_response(
        win,
        kb,
        image_data=image_data[probe_image_ind],
        probe_ori=probe_ori_start,
        trigger_kwargs_list=trigger_kwargs_list,
    )

    # get response error
    ang_error = circ_dist(probe_ori, final_ori)
    # normalized probe rating
    probe_rating = np.abs(ang_error) / 180
    # display to command line
    print(
        f"target ori.: {probe_ori:.2f}   final ori.: {final_ori:.2f}    angular error: {ang_error:.2f}"
    )

    # display a brief pause between probe and task report
    for frame in range(int(np.ceil(EXPECTED_FPS * 0.5))):
        win.flip()

    # =============================================================================
    # confidence probe or dummy task with no memory relevance
    # =============================================================================

    # prepare trigger list
    task_trigger_actions = ["task_prompt", "first_key_down", "task_timeout"]

    trigger_kwargs_list = [
        dict(ser=ser_port, tk=tk, value=ttl_dict[f"{action}"])
        for action in task_trigger_actions
    ]

    # get time stamp
    t_onset_task = rt_clock.getTime()

    # change task depending on block
    if task_name == "control":
        # initialze the wedge object
        target_width = trials["target_width"][itrial]
        target_ori = trials["target_ori"][itrial]
        # display the control task
        task_rating, task_resp_angle, task_keylog = display_control_wedge(
            win,
            kb,
            ctrl_width=0,
            ctrl_ori=final_ori,
            target_width=target_width,
            target_ori=target_ori,
            trigger_kwargs_list=trigger_kwargs_list,
        )
    elif task_name == "confidence":
        # set wedge object parameters to NaN instead
        target_width = np.nan
        target_ori = np.nan
        # display the confidence task
        task_rating, task_resp_angle, task_keylog = display_confidence_probe(
            win,
            kb,
            image_data=image_data[probe_image_ind],
            conf_width=0,
            conf_ori=final_ori,
            trigger_kwargs_list=trigger_kwargs_list,
        )

    # print performance
    print(
        f"{task_name:s} rating: {task_rating:.2f}    wedge angle: {task_resp_angle}"
    )

    # display a brief pause between probe and task report
    for frame in range(int(np.ceil(EXPECTED_FPS / 0.5))):
        win.flip()

    # =============================================================================
    # collect participant feedback and save to file
    # =============================================================================

    # define save dictionary
    savedict = {
        "trial": itrial,
        "task_name": task_name,  # which task was performed
        # save trial timings
        "iti_ms": iti_ms,
        "t_onset_trial": t_onset_trial,  # all timings are in seconds
        "t_onset_items": t_onset_items,
        "t_onset_delay": t_onset_delay,
        "t_onset_probe": t_onset_probe,
        "t_onset_task": t_onset_task,
        # save ids and oris of all relevant items
        "image_set": image_set,  # which set of images was used (constant for each subject)
        "n_items": len(image_indices),  # number of items per trial
        "item_ids": image_indices,  # item ids from image list
        "item_ori": image_orientations,  # item oris
        "probe_id": probe_image_ind,  # which image was probed
        "probe_idx_in_seq": probe_pos,  # which item from the sequence was probed
        "probe_ori_start": probe_ori_start,  # initial ori of probe item
        # save orientations and ratings of responses
        "probe_ori": probe_ori,  # true target item orientation
        "final_ori": final_ori,  # user ori response
        "ang_error": ang_error,  # error between response and true ori
        "probe_rating": probe_rating,  # normalized absolute response error
        "target_width": target_width * 360,  # angular width of target wedge on top
        "target_ori": target_ori,  # ori of ctrl task target wedge on top
        "task_resp_angle": task_resp_angle,  # task wedge angular width after response
        "task_rating": task_rating,  # normalized response angle of the second task (conf or ctrl)
    }

    # save to file
    save_dict(logfile, savedict)

    # save keylogs separately to not overcrowd the behavioral file
    keylog = {
        "trial": itrial,
        "probe_keylog": probe_keylog,  # pressed and released keys during item orientation
        "task_keylog": task_keylog,  # pressed and released keys during task
    }

    # save to file
    save_dict(keyfile, keylog)

    # =============================================================================
    # ITI interval
    # =============================================================================

    # =============================================================================
    # present block pause every nth trial (number of trials per block)
    # =============================================================================

    if (1 + itrial) % N_FEEDBACK_BLOCK == 0:
        print(f"present a block break every {N_FEEDBACK_BLOCK} trials")

        # compute the reward for the last block
        # parse task_name to reward function and this can then calculated reward based on
        # features for both tasks

        # get feedback break trigger
        trigger_kwargs["value"] = ttl_dict["feedback_break_begin"]
        # display a break at every few trials
        block_counter = display_block_break(
            win,
            kb,
            logfile=logfile,
            itrial=itrial,
            ntrials=ntrials,
            blocksize=N_FEEDBACK_BLOCK,
            block_counter=block_counter,
            hard_break=n_hard_break,
            task_name=task_name,
            next_task_name=next_task_name,
            eyetrack_dict=eyetrack_dict,
            trigger_kwargs=trigger_kwargs,
        )

        # trigger indicating end of break
        trigger_kwargs["value"] = ttl_dict["feedback_break_end"]
        send_trigger(**trigger_kwargs)


# %%
# send start experiment trigger
trigger_kwargs["value"] = ttl_dict["end_experiment"]
send_trigger(**trigger_kwargs)
core.wait(1)


# print monetary bonus to command line
if run_type == "experiment":
    # loads the non-training behavioral file and calculates bonus
    calculate_monetary_bonus(subjdir, subtype, substr)


# wrap up experiment
cleanup_experiment(win, kb, ser_port)


# Stop eye-tracking and get the data
print("Wait for eyetracker to save file...")
core.wait(15)
exit_eye_recording(
    eyetrack_dict["tk"],
    eyetrack_dict["edf_fname"],
    eyetrack_dict["edf_fname_local"],
)

# subprocess for EDF -> ASC conversion
print(f"Converting EDF to ASC ...\n\n     ({edf_fname_local})")
command = f"edf2asc {edf_fname_local}"
convert_edf2asc(command)

# end psychopy
core.quit()
