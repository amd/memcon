""" Provide utility functions for the main experiment. """

import numpy as np
import csv
import pandas as pd
from psychopy import core


def check_framerate(win, expected_fps):
    """ Get and check fps of this window """
    fps_counter = 0
    while True:
        fps = win.getActualFrameRate(nMaxFrames=1000)
        if fps is not None:
            fps = int(round(fps))
        if expected_fps == fps:
            return fps
        else:
            fps_counter += 1
            print(f"Found fps: {fps}, trying again.")
            core.wait(1)
        if fps_counter > 6:
            win.close()
            raise ValueError(f"Are you sure you are expecting {expected_fps} fps?")


def save_dict(fname, savedict):
    """Write dict to file.

    Parameters
    ----------
    fname : pathlib.Path
        The file location to write to. Will be created if it doesn't
        exist yet; else, content is appended.
    savedict : dict
        The data to write.
    """
    if not fname.exists():
        with open(fname, "w", newline="", encoding="utf-8") as fout:
            writer = csv.DictWriter(fout, savedict.keys(), delimiter="\t")
            writer.writeheader()
            writer.writerow(savedict)
    else:
        with open(fname, "a", newline="", encoding="utf-8") as fout:
            writer = csv.DictWriter(fout, savedict.keys(), delimiter="\t")
            writer.writerow(savedict)


def calculate_performance(logfile, blocksize=None):
    """Calculate accuracy as percent correct choices overall and of last block.

    Parameters
    ----------
    logfile : pathlib.Path
        Path object pointing to the logfile with behavioral data
    blocksize : int | None (default)
        How many trials fit into one block. If None, then computes the accuracy
        for all trials in the saved dict.

    Returns
    -------
    acc_overall, acc_block : int
        Accuracy as percentage correct choices overall so far, and in the last block.
    """

    # predefine which columns to load, must be in order of columns in file!
    colnames = ["task_name", "probe_rating", "target_width", "task_resp_angle"]
    # load the save dict
    df = pd.read_csv(logfile, sep="\t", usecols=colnames)

    # select only data from the last block
    if not blocksize is None:
        df = df.iloc[-blocksize:]

    # normalize task wedge angle to circle, depending on task is either conf or ctrl
    df["target_width"] = df["target_width"] / 360
    df["task_resp_angle"] = df["task_resp_angle"] / 360

    # print of which tasks we compute the performance. During the experiment it should just be one
    print(np.unique(df["task_name"]))

    # initialize the task accuracy
    task_acc = np.zeros((len(df),))

    # Since the task is different when computing the bonus for all trials at the end, we need
    # to loop over the trials
    for itrial, (_, row) in enumerate(df.iterrows()):
        # unpack row values
        task, probe, target, resp = row

        # compute task accuracy
        if task == "confidence":
            # stepwise function
            accuracy = 1 - (resp - probe)
        elif task == "control":
            # same stepwise function
            accuracy = 1 - (resp - target)

        if accuracy > 1:
            accuracy = 0

        task_acc[itrial] = accuracy

    # compute final performance score
    final_score = (1 - df["probe_rating"]) * task_acc

    return final_score


def calculate_experiment_bonus(logfile):
    """ Calculates the bonus earned in the entire experiment across both tasks """


def circ_dist(x, y):
    """computes the signed circular distance between two
    angles x and y in degree """

    d = (x - y + 180) % 360 - 180

    return d
