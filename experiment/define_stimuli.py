""" Define stimuli for experiment. """

import os
import numpy as np

from PIL import Image
from psychopy import visual
from psychopy.visual.pie import Pie

from define_settings import (
    NIMAGE_SUBSETS,
    path_to_images,
    WEDGE_SIZE_DVA,
)


def get_central_text_stim(win, height, text="", pos=(0, 0), color=0.2):
    """Get a central text stimulus to use e.g., for the block break."""

    text_stim = visual.TextStim(
        win=win,
        height=height,
        units="deg",
        font="Helvetica Neue",
        anchorVert="center",
        text=text,
        pos=pos,
        alignText="center",
        anchorHoriz="center",
        color=color,
    )

    return text_stim


def get_exp_text_stim(win, height, text="", pos=(0, 0.65), color=0.3):
    """Get an experimenter text stimulus that is presented more towards the top
    of the screen to make it a bit easier for experimenters and participants to
    distinguish between text that is only important for either of the two.
    """

    text_stim = visual.TextStim(
        win=win,
        height=height,
        units="deg",
        font="Helvetica Neue",
        anchorVert="center",
        text=text,
        pos=pos,
        alignText="center",
        anchorHoriz="center",
        color=color,
    )

    return text_stim


def get_fixation_stim(win, stim_type="dot"):
    """ Get the psychopy objects for the fixation stimulus.
    This can be dot or cross.
    
    Parameters
    ----------
    win : psychopy.visual.Window
        The psychopy window on which to draw the fixation stimulus.
    stim_type : str | dot (default)
        the type of fixation stimulus
    
    Returns
    -------
    stim : tuple
        contains the objects that collectively encompass the chosen fixation
        stimulus type. The output here is a tuple so it can be iterated over later.
    """

    if stim_type == "dot":
        stim = get_fixation_dot(win=win)
    elif stim_type == "cross":
        stim = get_fixation_cross(win=win)

    return stim


def get_fixation_dot(win, color=(-1, -1, -1)):
    """ Draws a central dot for fixation on the screen
    
    Parameters
    ----------
    win : psychopy.visual.Window
        The psychopy window on which to draw the fixation stimulus
    color : tuple
        Color of the dot (-1=black, 0=gray, 1=white)
    
    Returns
    -------
    dot : psychopy dot object
        the central dot
    """

    dot = visual.Circle(
        win=win,
        radius=0.1,
        edges=8,
        units="deg",
        fillColor=color,
        lineColor=color,
    )

    return (dot,)


def get_fixation_cross(win, back_color=(-1, -1, -1), stim_color=(0, 0, 0)):
    """Provide objects to represent a fixation stimulus as in [1]_.

    Parameters
    ----------
    win : psychopy.visual.Window
        The psychopy window on which to draw the fixation stimulus.
    back_color : tuple
        Color of the background (-1=black, 0=gray, 1=white).
    stim_color : tuple
        Color of the stimulus (-1=black, 0=gray, 1=white).

    Returns
    -------
    outer, inner, horz, vert : tuple of psychopy Circle and Rect objects
        The objects that make up the fixation stimulus.

    References
    ----------
    .. [1] Thaler, L., Schütz, A. C., Goodale, M. A., & Gegenfurtner, K. R.
       (2013). What is the best fixation target? The effect of target shape on
       stability of fixational eye movements. Vision Research, 76, 31-42.
       https://www.doi.org/10.1016/j.visres.2012.10.012

    """
    # diameter outer circle = 0.6 degrees
    # diameter circle = 0.2 degrees
    outer = visual.Circle(
        win=win,
        radius=0.6 / 2,
        edges=32,
        units="deg",
        fillColor=stim_color,
        lineColor=back_color,
    )

    inner = visual.Circle(
        win=win,
        radius=0.2 / 2,
        edges=32,
        units="deg",
        fillColor=stim_color,
        lineColor=stim_color,
    )

    horz = visual.Rect(
        win=win,
        units="deg",
        width=0.6,
        height=0.2,
        fillColor=back_color,
        lineColor=back_color,
    )

    vert = visual.Rect(
        win=win,
        units="deg",
        width=0.2,
        height=0.6,
        fillColor=back_color,
        lineColor=back_color,
    )

    return (outer, inner, horz, vert)


def load_images(subid):
    """ Loads all image stimuli as numpy arrays and returns them in a list 
    
    Parameters
    ----------
    subid : int
        the number rank of the participant by which we select one of the
        three image subsets
    
    Returns
    -------
    image_data : list
        list of np.ndarrays containing the images
    image_set : int
        which set of images we use for this participant as stimuli
    """

    # check input, if 'test' then set subid to one
    if isinstance(subid, str):
        subid = 1

    # path to stimulus images
    image_set = subid % NIMAGE_SUBSETS
    img_path = path_to_images / f"{image_set:02d}"
    # list image names
    image_list = [fname for fname in os.listdir(img_path) if not fname.startswith(".")]
    image_list = np.sort(image_list)

    # initilize image list
    image_data = []

    # load all images in list of names
    for image_name in image_list:
        image_fname = img_path / image_name

        # load image
        pil_image = Image.open(image_fname)
        # convert to numpy array with shape width, height, channels, convert to float in 0--1 range
        image_np = np.array(pil_image, order="C").astype(float) / 255.0
        # flip image (row-axis upside down so we need to reverse it)
        image_np = np.flip(image_np, axis=0)

        image_data.append(image_np)

    # cover to numpy array so we can use vector to index
    image_data = np.array(image_data)

    return image_data, image_set


def get_image_stim(window, image_data, orientations, size, opacity=1.0):
    """ load and return the visual.Image objects of the image 
    
    Parameters
    ----------
    window : psychopy.visual.Window
        The psychopy window on which to draw the stimuli.
    image_data : list
        list of np.ndarrays of images that we want to cast into psychopy objects
    orientation : int 
        orientation in degrees by which the object should be rotated
    
    Returns
    -------
    image_stims : list
        list of psychopy.visual.ImageStim objects of the images
        we want to display
    """

    # if only one image is requested, put orientations into list
    if not isinstance(orientations, np.ndarray):
        orientations = np.array([orientations])

    # initialize list for images
    image_stims = []

    for i_image, image_np in enumerate(image_data):
        # cast numpy array into psychopy object
        image = visual.ImageStim(
                                win=window,
                                image=image_np,
                                units="deg",
                                # need to pass the size (x, y) explicitly
                                # img_as_float converts to 0:1 range,
                                # whereas PsychoPy defaults to -1:1
                                size=(
                                    size, size
                                    ),
                                colorSpace="rgb1",
                                opacity=opacity,
                                )

        # set image orientation
        image.ori = orientations[i_image]

        image_stims.append(image)

    return image_stims


def get_confidence_bar_stim(win, cursor_xinit, verbose=False):
    """ creates a horizontal bar on the screen with a perpendicular cursor
    which the user can change

    Parameter
    ---------
    win : psychopy.visual.window
        the window in which we display the bar
    cursor_xinit : float
        the position of the cursor on the bar
        right now this is in pixel, but for later we want to make everything
        relative to the screen inside this function and only parse the
        interpreted values
    verbose : bool | False (default)
        print the cursor position value on top (for calibration purposes)
    
    Returns
    -------
    rating_line : psychopy.visual.Line
        the rating line
    cursor_line : psychopy.visual.Line
        the vertical cursor line
    """

    # get the window size
    win_x, win_y = win.size
    # define the maxima of the rating scale in pixels, from center (0, 0)
    slider_xpos = win_x / 8
    cursor_ypos = win_y / 32
    # map the initial cursor value to a pixel position
    cursor_xpos = (2 * slider_xpos) * cursor_xinit

    # initialze the line object
    rating_line = visual.Line(win,
                        units="pix",
                        lineColor=(-1, -1, -1),
                        lineWidth=15,
                        )
    rating_line.start = [-slider_xpos, 0]
    rating_line.end = [slider_xpos, 0]

    # cursor line object
    cursor_line = visual.Line(win,
                              units="pix",
                              lineColor=(-1, -1, -1),
                              lineWidth=10,
                              )
    cursor_line.start = [cursor_xpos - slider_xpos, cursor_ypos]
    cursor_line.end = [cursor_xpos - slider_xpos, -cursor_ypos]


    return (rating_line, cursor_line)


def get_confidence_wedge(win, init, center, color=(0.2, 0.2, 0.2), opacity=1.0,
                         linecolor=(0, 0, 0)):
    """ Creates a circle wedge with a given angular width repsented on
    the screen.
    
    Parameters
    ----------
    win : psychopy.visual.window
        the window in which we display the bar
    center : float
        the position of the cursor on the bar.
        interpreted values
    color : tuple | (0.2, 0.2, 0.2) (default)
        the color of the wedge
    opacity : float | 1.0 (default)
        the wedge opacity. Must be a value between 0 and 1.
    linecolor : tuple | (0, 0, 0) (default)
        the color of the wedge contour
    verbose : bool | False (default)
        print the cursor position value on top (for calibration purposes)
    
    Returns
    -------
    wedge : psychopy.visual.pie.Pie object
    """

    # map the initial value to degrees
    angle = init * 360
    # get start and end angles of pie wedge
    pie_start = center - angle / 2
    pie_end = center + angle / 2

    # create wedge object
    wedge = Pie(
        win,
        units="deg",
        start=pie_start,
        end=pie_end,
        size=WEDGE_SIZE_DVA,
        fillColor=(*color, opacity),
        lineColor=linecolor,
    )

    return wedge


def get_ctrl_target_wedge(win, init, center, opacity=1.0):
    """ Get the target wedge for the ctrl task, which sits at the top and has a random
    size that participants should adjust the ctrl wedge to.
    
    consider if it also should have a random orientation. otherwise could bias the probe
    orientation before the ctrl task to be more upright, so that the task gets easier?
    
    Parameters
    ----------
    win : psychopy.visual.window
        the window in which we display the bar
    init : float
        the ctrl wedge size
    
    Returns
    -------
    wedge : psychopy.visual.pie.Pie object
    """

    # map the initial value to degrees
    angle = init * 360
    # get start and end angles of pie wedge
    pie_start = center - angle / 2
    pie_end = center + angle / 2

    # create wedge object
    wedge = Pie(
        win,
        pos=(0.0, 10),
        size=1.5,
        units="deg",
        start=pie_start,
        end=pie_end,
        fillColor=(0.2, 0.2, 0.2, opacity),
        lineColor=(0, 0, 0),
    )

    return wedge
