"""Definitions for the TTL triggers to be sent.

We have a "single" and a "dual" task, which will be performed in separate EEG recordings
that are then optionally concatenated for analysis.
Both tasks will have the same number of events,
hence we will use the same number of TTL triggers.
However all TTL trigger codes for the "dual" task will be incremented by a constant.

"""


def send_trigger(ser, tk, value):
    """Send an event code to serial and eye-tracker."""
    ser.write(value)
    tk.sendMessage(f"ethernet{value}")


def get_ttl_dict():
    """Provide a dictionnary mapping str names to byte values."""
    ttl_dict = {}

    # At the beginning and end of the experiment ... take these triggers to
    # crop the meaningful EEG data. Make sure to include some time BEFORE and
    # AFTER the triggers so that filtering does not introduce artifacts into
    # important parts.
    ttl_dict["begin_experiment"] = 80
    ttl_dict["end_experiment"] = 90

    # Trial structure triggers
    # New trial starts
    ttl_dict["new_trl"] = 1

    # first fixstim offset in trial
    ttl_dict["fixstim_offset"] = 2

    # prompt a response
    ttl_dict["response_prompt"] = 3
    ttl_dict["task_prompt"] = 4

    # maintenante interval
    ttl_dict["maintenance"] = 5

    # feedback: block break
    ttl_dict["feedback_break_begin"] = 6
    ttl_dict["feedback_break_end"] = 7
    ttl_dict["offset_block_break"] = 8

    # Stimulus specific triggers
    # first item in sequence
    ttl_dict["seq_0_item_0"] = 100
    ttl_dict["seq_0_item_1"] = 101
    ttl_dict["seq_0_item_2"] = 102

    # first item in sequence
    ttl_dict["seq_1_item_0"] = 103
    ttl_dict["seq_1_item_1"] = 104
    ttl_dict["seq_1_item_2"] = 105

    # first item in sequence
    ttl_dict["seq_2_item_0"] = 106
    ttl_dict["seq_2_item_1"] = 107
    ttl_dict["seq_2_item_2"] = 108

    # Response specific triggers
    # participant took to long to respond
    ttl_dict["response_timeout"] = 30
    ttl_dict["task_timeout"] = 31

    # valid participant respones
    ttl_dict["first_key_down"] = 32
    ttl_dict["key_left"] = 33
    ttl_dict["key_right"] = 34
    ttl_dict["key_return"] = 35

    return ttl_dict


class FakeSerial:
    """Convenience class to run the code without true serial connection."""

    def write(self, value):
        """Take a value and do nothing."""
        return value

    def close(self):
        """Do nothing."""
        return


class MySerial:
    """Convenience class that always resets the event marker to zero."""

    def __init__(self, ser, wait_microsecs):
        """Take a serial object, and a time to wait before resetting.

        Parameters
        ----------
        ser : str | serial.Serial | FakeSerial
            A (optionally "fake") serial port object or an address of a serial port.
        wait_microsecs : float
            Time in microseconds to wait until resetting the serial port to zero.

        Notes
        -----
        See: http://www.labhackers.com/usb2ttl8.html
        """
        import serial

        if isinstance(ser, (serial.Serial, FakeSerial)):
            self.ser = ser
        else:
            self.ser = serial.Serial(port=ser, baudrate=128000, timeout=0.01)

            # Set USB2TTL8 device to write mode
            self.ser.write(b"SET DATA_MODE WRITE\n")
            while self.ser.readline():
                # "empty" the buffer
                pass

            # Set data to 0
            self.ser.write(b"WRITE 0\n")

        self.wait_microsecs = wait_microsecs
        self.reset_value = 0

    def write(self, value):
        """Take a value, write it, and reset to zero."""
        # see: http://www.labhackers.com/usb2ttl8.html
        send_str = f"WRITE {value} {self.wait_microsecs} {self.reset_value}\n"
        self.ser.write(send_str.encode())  # convert to bytes before sending
