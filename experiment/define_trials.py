"""Define trials for the experiment."""

import numpy as np
from define_settings import (
    BLOCK_DESCR,
    LOAD_CONDS,
    N_LOAD_CONDS,
    ORIENTATIONS,
)


def gen_trial(rng, n_images, n_images_per_trial, orientations):
    """Generate trials for a participant.

    A trial consists out of `nsamples` samples, which are
    digits between 1 and 9 that are of two colors - here
    indicated by the sign (negative or positive). Each trial
    contains exactly ``nsamples/2`` samples of each color.

    Parameters
    ----------
    rng : np.random.Generator
        The random number generator object based on which to
        generate the trials.
    n_images : int
        The number of images from which we randomly choose
    n_images_per_trial : int
        The number of images we present per trial

    Returns
    -------
    samples : np.ndarray, shape(nsamples,)
        Samples for this trial.
    img_oris : np.ndarray
        The randomly sampled image orientations in degree
    """
    # images indices
    images = np.arange(0, n_images, dtype=int)

    # all drawn from uniform distribution
    samples = rng.choice(images, n_images_per_trial, replace=False)
    # randomly sample the orientations
    img_oris = rng.choice(orientations, n_images_per_trial, replace=False)

    # randomly chose the probe item ID, based on the number of imager per trial
    probe_id = rng.choice(n_images_per_trial)
    # same for the orientation
    probe_ori = rng.choice(orientations)

    # create the width and orietation of the control task target wedge on top
    target_width = 0.1 + np.random.rand() * 0.8  # uniformly sampled between 0.1 and 0.9
    target_ori = rng.choice(orientations)

    return samples, img_oris, probe_id, probe_ori, target_width, target_ori


def gen_all_trials(
    subid, n_images, BLOCKSIZE, NBLOCKS, NTRIALS_PER_COND, NTRIALS, seed=None
):
    """Takes variables specifying blocks, block order, number of trials and so on from
    the define_settings and creates the list of trials.

    Parameters
    ----------
    subid : int
        the rank number of the participant by which we order the tasks
    n_images : int
        the number of images from which we randomly choose
    NBLOCKS : int
        number of task blocks
    NSAMPELS_PER_COND : int
        number of trials per condition
    NTRIALS : int
        total number of trials
    seed : int | None (default)
        The seed for the random number generator.

    Returns
    -------
    trials : list
        list of trials with their respective parameters
    """
    # check input, if test then make subid one
    if isinstance(subid, str):
        subid = 1

    # revert order of tasks for uneven subject IDs
    if (subid % 2) != 0:
        BLOCK_DESCR.reverse()

    # define random Generator
    rng = np.random.default_rng(seed)

    # first get the task block order
    block_sequence = BLOCK_DESCR
    # then repeat block description for each trial in that block
    block_sequence = list(np.repeat(block_sequence, BLOCKSIZE))

    # define the number of items per trials
    load_seq_per_block = [LOAD_CONDS * NTRIALS_PER_COND] * NBLOCKS
    # get maximum number of items per trial
    max_n_items = np.max(LOAD_CONDS)

    # for each block, create the trials in random order
    for i_seq, seq in enumerate(load_seq_per_block):
        load_seq_per_block[i_seq] = list(rng.permutation(seq))
    # concatenate blockwise load sequence
    load_sequence = sum(load_seq_per_block, [])

    # create trial dict
    trials = {
        "block_sequence": block_sequence,
        "load_sequence": load_sequence,
        "img_id": np.nan * np.zeros((NTRIALS, max_n_items)),
        "img_ori": np.nan * np.zeros((NTRIALS, max_n_items)),
        "probe_id": np.nan * np.zeros((NTRIALS, 1)),
        "probe_ori": np.nan * np.zeros((NTRIALS,)),
        "target_width": np.nan * np.zeros((NTRIALS,)),
        "target_ori": np.nan * np.zeros((NTRIALS,)),
    }

    # create parameters for each trial
    for idx in range(NTRIALS):
        # get number of items for this trial
        n_stim_trial = load_sequence[idx]
        # generate the trial
        (
            trials["img_id"][idx, :n_stim_trial],
            trials["img_ori"][idx, :n_stim_trial],
            trials["probe_id"][idx, :],
            trials["probe_ori"][idx],
            trials["target_width"][idx],
            trials["target_ori"][idx],
        ) = gen_trial(
            rng=rng,
            n_images=n_images,
            n_images_per_trial=n_stim_trial,
            orientations=ORIENTATIONS,
        )

    # print parameters to command line
    print("\nGenerated Trials with the following Parameters:")
    print(f"Task order:                         {BLOCK_DESCR}")
    print(f"Load conditions:                    {LOAD_CONDS}")
    print(f"Number of trials per condition:     {NTRIALS_PER_COND}")
    print(f"Total number of trials:             {NTRIALS} \n")

    return trials


# ===============================================================================
# new test code to create orthogonalized trials


def gen_balanced_trial(rng, probe_ori, probe_id, n_images, n_images_per_trial, orientations):
    """Generate trials for a participant based on the item orientation.
    This means that the orientations of the other items are freely choosen
    at random, but excluding the target item orientation.


    A trial consists out of `nsamples` samples, which are
    digits between 1 and 9 that are of two colors - here
    indicated by the sign (negative or positive). Each trial
    contains exactly ``nsamples/2`` samples of each color.

    Parameters
    ----------
    rng : np.random.Generator
        The random number generator object based on which to
        generate the trials.
    probe_ori : float
        the orientation of the target item that we want to exclude from
        the other items in the sequence
    probe_id : int
        the ID of the target item that we want to exclude from
        the other items in the sequence
    n_images : int
        The number of images from which we randomly choose
    n_images_per_trial : int
        The number of images we present per trial

    Returns
    -------
    img_ids : np.ndarray, shape (nsamples,)
        Sample IDs for this trial based on files in stimuli folder.
    img_oris : np.ndarray
        The randomly sampled image orientations in degree, with the probe
        stimulus being balanced across trials
    probe_pos : int
        the position of the to be probed item in the sequence, the sequence
        index
    probe_id : int
        the image id of the to be probed item
    probe_ori : float
        the orientation value in degrees of the to be probed item
    probe_ori_start : float
        the orientation value in degrees of the to be probed item at the start of the test
    target_width : float
        the width of the control task target wedge on top of the screen
    target_ori : float
        the orientation of the control task target wedge on top of the screen
    """

    # create images indices
    images = np.arange(0, n_images, dtype=int)
    # randomly chose the position of the target item in the sequence
    probe_pos = rng.choice(n_images_per_trial)

    # remove the probe orientation from orientations from which we draw the rest
    orientations = np.delete(orientations, orientations == probe_ori)
    images = np.delete(images, images == probe_id)

    # set index for randomly chosen other items
    pos_index = np.r_[:probe_pos, probe_pos+1:n_images_per_trial]
    # randomly choose indices of the other items
    img_ids = np.zeros((n_images_per_trial,))
    img_ids[pos_index] = rng.choice(images, n_images_per_trial - 1, replace=False)
    img_ids[probe_pos] = probe_id
    # randomly choose orientations of the other items
    img_oris = np.zeros((n_images_per_trial,))
    img_oris[pos_index] = rng.choice(orientations, n_images_per_trial - 1, replace=False)
    img_oris[probe_pos] = probe_ori

    # randomly choose the orientation of the probe when it is presented at the start of the test
    probe_ori_start = rng.choice(orientations)

    # create the width and orietation of the control task target wedge on top
    # also serves as the orientation at probe onset
    target_width = 0.1 + np.random.rand() * 0.8  # uniformly sampled between 0.1 and 0.9
    target_ori = rng.choice(orientations)

    return img_ids, img_oris, probe_pos, probe_id, probe_ori, probe_ori_start, target_width, target_ori


def gen_all_balanced_trials(
    subid,
    n_images,
    nrepeats_per_ori,
    BLOCKSIZE,
    NBLOCKS,
    NTRIALS_PER_COND,
    NTRIALS,
    seed=None,
):
    """Generates all trials by balancing the target item orientation with
    the load conditions, such that each orientation is repeated the same numer
    of times within each load condition and task.

    To validate, I have generated balanced trials and checked if for each load condition
    each orientation occurs the same number of times in the probe_ori field.
    The table of these trials is:

    balanced condition trials (for one orientation condition and one task)
    ======================================================================
    |    load    |    ori    |    id    |
    |  ========  | ========= | ======== |
    |     1      |   11.25   |     1    |
    |     2      |   11.25   |     1    |
    |     3      |   11.25   |     1    |
    |     1      |   11.25   |     1    |
    |     2      |   11.25   |     2    |
    |     3      |   11.25   |     2    |
    |     1      |   11.25   |     2    |
    |     2      |   11.25   |     2    |
    |     3      |   11.25   |     3    |
    |     1      |   11.25   |     3    |
    |     2      |   11.25   |     3    |
    |     3      |   11.25   |     3    |

    This table is repeated for each orientation (e.g. 33.75, 56.25, etc.) and each task (e.g.
    confidence and control tasks). The trials in this experiment are relatively long with
    about 16 seconds, due to two response tasks, and therefore we cannot do too many trials
    and we cannot balance all load conditions, orientations and IDs in a perfectly counter-
    balanced design. As is seen in the table here, we therefore balance the load conditions
    with the orientations (which are the main focus of analysis) and the orientations with
    the item IDs. The IDs are not balanced with the load and this means that each ID occurs
    TWICE in ONE LOAD condition and ONCE IN THE OTHER load conditions!
    Let's hope this will not fuck up the design...

    Some other parameters are not balanced and instead chosen at random. These include:
        the position of the to be probed item in the sequence (probe_pos)
        the control task wedge size (target_width)
        the orientation of the wedge in the control task (target_ori)


    Parameters
    ----------
    subid : int
        the rank number of the participant by which we order the tasks
    n_images : int
        the number of images from which we randomly choose
    nrepeats_per_ori : int
        number of orientation repetitions within the same load condition and task
    NBLOCKS : int
        number of task blocks
    NSAMPELS_PER_COND : int
        number of trials per condition
    NTRIALS : int
        total number of trials
    seed : int | None (default)
        The seed for the random number generator.

    Returns
    -------
    trials : dict
        dict of trial parameters with a list of their respective values per trial
    """
    # check input, if test then make subid one
    if isinstance(subid, str):
        subid = 1
    # revert order of tasks for uneven subject IDs
    if (subid % 2) != 0:
        BLOCK_DESCR.reverse()

    # define random Generator
    rng = np.random.default_rng(seed)

    # first get the task block order
    block_sequence = BLOCK_DESCR
    # then repeat block description for each trial in that block
    block_sequence = list(np.repeat(block_sequence, BLOCKSIZE))

    # define the number of items per trials
    load_seq_per_block = [LOAD_CONDS * NTRIALS_PER_COND] * NBLOCKS
    # get maximum number of items per trial
    max_n_items = np.max(LOAD_CONDS)

    # repeat the orientations n number of times
    ori_vec = np.repeat(ORIENTATIONS, nrepeats_per_ori * N_LOAD_CONDS)[:, np.newaxis]
    # item id vector
    id_vec = np.array(range(n_images))[np.newaxis,:]
    id_vec = np.repeat(id_vec, nrepeats_per_ori, axis=0).T.reshape(-1, 1)
    id_vec = np.tile(id_vec, (len(ORIENTATIONS), 1))
    # for each block, create the trials in random order
    for i_seq, seq in enumerate(load_seq_per_block):
        # concatenate the load condition vector and the orientation, then shuffle
        seq_trials = np.concatenate((np.array(seq)[:, np.newaxis], ori_vec, id_vec), axis=1)
        load_seq_per_block[i_seq] = list(rng.permutation(seq_trials, axis=0))
    # concatenate task blocks
    load_sequence = np.concatenate((load_seq_per_block), axis=0)

    # create trial dict
    trials = {
        "block_sequence": np.array(block_sequence),
        "load_sequence": load_sequence[:, 0],
        "img_id": np.nan * np.zeros((NTRIALS, max_n_items)),
        "img_ori": np.nan * np.zeros((NTRIALS, max_n_items)),
        "probe_pos": np.nan * np.zeros((NTRIALS,)),
        "probe_id": np.nan * np.zeros((NTRIALS, 1)),
        "probe_ori": np.nan * np.zeros((NTRIALS,)),
        "probe_ori_start": np.nan * np.zeros((NTRIALS,)),
        "target_width": np.nan * np.zeros((NTRIALS,)),
        "target_ori": np.nan * np.zeros((NTRIALS,)),
    }

    # create parameters for each trial
    for idx, (n_stim_trial, probe_ori, probe_id) in enumerate(load_sequence):
        # get number of items for this trial
        n_stim_trial = int(n_stim_trial)
        # generate the trial
        (
            trials["img_id"][idx, :n_stim_trial],
            trials["img_ori"][idx, :n_stim_trial],
            trials["probe_pos"][idx],
            trials["probe_id"][idx, :],
            trials["probe_ori"][idx],
            trials["probe_ori_start"][idx],
            trials["target_width"][idx],
            trials["target_ori"][idx],
                    ) = gen_balanced_trial(
                        rng=rng,
                        probe_ori=probe_ori,
                        probe_id=probe_id,
                        n_images=n_images,
                        n_images_per_trial=n_stim_trial,
                        orientations=ORIENTATIONS,
                    )

    # print parameters to command line
    print("\nGenerated Trials with the following Parameters:")
    print(f"Task order:                         {BLOCK_DESCR}")
    print(f"Load conditions:                    {LOAD_CONDS}")
    print(f"Number of trials per condition:     {NTRIALS_PER_COND}")
    print(f"Total number of trials:             {NTRIALS} \n")

    return trials


def subselect_trials(trials, ntrials):
    """Selects a number of trial per task from the trals dict
    
    Parameters
    ----------
    trials : dict
        trials as returned from `gen_all_balanced_trials`
    ntrials : int
        number of trials we want to select per task
    
    Returns
    -------
    trials
    """

    # check that n trials is not more than we have in trials
    assert ntrials <= len(trials["img_id"]), "Too many trials requested"

    # select some trials per task, first and last ntrials
    train_trial_index = np.r_[:int(ntrials/2), -int(ntrials/2):0]
    trials = {key : val[train_trial_index, ...] for key, val in trials.items()}

    return trials
