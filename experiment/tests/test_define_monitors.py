""" Test importing monitors and return the list of available ones.
"""

import os
import subprocess

from psychopy import monitors

if __name__ == "__main__":
    # call define monitors and check that they get defined
    cmd = ["python", f"experiment{os.sep}define_monitors.py"]
    subprocess.call(cmd)
    monitor_list = monitors.getAllMonitors()
    # check that either of the chosen ones are in the list
    print("assert monitors are in list...")
    for mon in ["eizoforis", "latitude"]:
        assert mon in monitor_list, f"not present {mon}"
