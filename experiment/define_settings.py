""" Provide constants for several settings in the experiment. """

import os
from pathlib import Path
import numpy as np

# pick a monitor
monitor = "eizoforis"  # benq, latitude, eizoforis
MONITOR_NAME, EXPECTED_FPS = {
    "benq": ("benq", 144),
    "latitude": ("latitude", 60),
    "eizoforis": ("eizoforis", 144),
    "U28D590" : ("U28D590", 30),
    "testMonitor" : ("testMonitor", 120),
}[monitor]

# select the keyboard backend for the experiment
if monitor == "testMonitor":
    kb_backend = "event"
else:
    kb_backend = "ptb"

# Serial port for sending TTL triggers to EEG
# INSTRUCTIONS TRIGGER BOX
# https://www.brainproducts.com/downloads.php?kid=40
# Open the Windows device manager,
# search for the "TriggerBox VirtualSerial Port (COM4)"
# in "Ports /COM & LPT)" and enter the COM port number in the constructor.
# If there is no TriggerBox, set SER_ADDRESS to None
SER_WAIT_MICROSECS = 5000  # depending on sampling frequncy: at 1000Hz, must be >= 0.001s
ser_auto_determine = True  # set to False if on Win, and no Serial Port wanted
SER_ADDRESS = None
if ser_auto_determine and os.name == "nt":
    SER_ADDRESS = "COM5"


# Eye-tracker settings
CALIBRATION_TYPE = "HV5"
tk_auto_determine = False  # set to False if on Win, and no EyeLink wanted.
TK_DUMMY_MODE = False
if tk_auto_determine and os.name == "nt":
    # if on Windows, use EyeLink
    TK_DUMMY_MODE = False


# if we want to check the screen frame rate before running
checkTiming = False

path_to_images = Path("stimuli/")
# get number of subsets, should be 3
NIMAGE_SUBSETS = len([f for f in os.listdir(path_to_images) if not f.startswith('.')])
NIMAGES = 3 # we always assume to have a triplet

# the number of possible orientations on a circle
NORI = 16
# create the orientation conditions with a offset of half a condition to avoid cardinals
ORIENTATIONS = np.linspace(0, 360, NORI, endpoint=False) + 180/NORI

# Define stimuli lengths
MIN_ITI_MS = 1000
MAX_ITI_MS = 2500

# times we want to *show* and *fade* a digit. Try to get as close as possible, given FPS
image_ms = 350
isi_ms = 650
maintenance_ms = 3600
IMAGE_FRAMES = int(np.round(image_ms / (1000 / EXPECTED_FPS)))
ISI_FRAMES = int(np.round(isi_ms / (1000 / EXPECTED_FPS)))
MAINTENANCE_FRAMES = int(np.round((maintenance_ms / (1000 / EXPECTED_FPS))))

# number off frames we wait between ITI and start of stimuli
FIXSTIM_OFF_FRAMES = int(np.ceil(EXPECTED_FPS / 2))
# maximum time in seconds to wait for the responses
max_wait_response = 4
WAIT_RESPONSE_PROBE_FRAMES = int(np.ceil(EXPECTED_FPS * max_wait_response))

# size of the images
IMAGE_SIZE_DVA = 8 # in degrees
WEDGE_SIZE_DVA = 8 # in degrees
# image, but it should scale positively to an actual size
TEXT_HEIGHT_DVA = 1
# orientation response acceleartion on button press
ROT_ACCELERATION = 7 # degrees per second


# block decription - judge confidence or control task
BLOCK_DESCR = ["confidence", "control"]
# total number of blocks
NBLOCKS = len(BLOCK_DESCR)
# load condition by number of items presented
LOAD_CONDS = [1, 2, 3]
N_LOAD_CONDS = len(LOAD_CONDS)
# number of conditions
N_CONDS = NBLOCKS * N_LOAD_CONDS


# number of orientation repetitions for each load condition
NREPEATS_PER_ORI = 4
# number of repetitions per block
NTRIALS_PER_COND = NREPEATS_PER_ORI * NORI # number of trials per condition (task AND load)
# resulting in 360 trials in total
NTRIALS = int(N_CONDS * NTRIALS_PER_COND)

# number of trials per task block = 180
BLOCKSIZE = int(NTRIALS / NBLOCKS)

# number of trials per elf-paced block
N_FEEDBACK_BLOCK = int(NTRIALS_PER_COND / 2) # how many trials per feedback block
n_hard_break = 2 # how many feedback blocks until hard break


# if experiment should run in fullscreen
FULLSCR = True


# maxmimum reward in Euro that can be achieved in a single trial with perfect performance
MAX_REWARD = 15 # little more than 4 cents per trial
REWARD_CONSTANT = MAX_REWARD / NTRIALS


# acceptable keys to respond for actions "left", "right", "up" and "quit"
# allows for left handed participants to use the letter keys
KEYLIST_DICT = dict(
    left=["left", "a"],
    right=["right", "d"],
    up=["up", "w"],
    quit=["q"],
)


# graphics parameters
# confidence bar thickness
