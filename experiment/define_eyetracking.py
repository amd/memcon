"""Define eye-tracking for the experiment.

See also the eye_tracking directory for more information.
"""

import subprocess

from define_settings import TEXT_HEIGHT_DVA

from define_stimuli import get_central_text_stim


class DummyEyeLink:
    """Convenience class to run the code without true EyeLink connection."""

    def sendMessage(self, msg):
        """Take a message string and return it (do nothing)."""
        return msg


def update_status(tk, block, max_block, trial, max_trial):
    """Update the 'title' field on the Eyelink Host PC display."""
    if isinstance(tk, DummyEyeLink):
        return
    tk.sendCommand(
        f"record_status_message 'block {block} of {max_block};"
        f" trial {trial} of {max_trial}'"
    )


def start_eye_recording(tk):
    """Start eye-tracking."""
    if isinstance(tk, DummyEyeLink):
        return 0
    import pylink

    tk.setOfflineMode()
    error = tk.startRecording(1, 1, 1, 1)
    pylink.pumpDelay(100)
    return error


def stop_eye_recording(tk):
    """Stop eye-tracking."""
    if isinstance(tk, DummyEyeLink):
        return 0
    import pylink

    pylink.pumpDelay(100)
    tk.stopRecording()


def exit_eye_recording(tk, edf_fname, edf_fname_local):
    """Exit eye-tracking and transfer an EDF file to local PC."""
    if isinstance(tk, DummyEyeLink):
        return
    import pylink

    print("\n\n... Transferring eyetracking data ...\n\n")
    stop_eye_recording(tk)
    tk.setOfflineMode()
    tk.closeDataFile()
    pylink.pumpDelay(100)
    tk.receiveDataFile(edf_fname, edf_fname_local)
    tk.close()


def setup_eyetracker(dummy_mode, mon, edf_fname, calibration_type):
    """Do setup and calibrate the EyeLink.

    A well documented version of this setup is described in the try_eyelink.py
    script in the eye_tracking/ directory.

    Parameters
    ----------
    dummy_mode : bool
        Whether or not to run in dummy mode.
    mon : psychopy.monitors.Monitor
        The monitor object to use for calibration.
    edf_fname : str
        The name of the file to save the eyetracking data to.
    calibration_type : {"HV5", "HV9"}
        The type of calibration to perform.

    Returns
    -------
    tk : DummyEyeLink | EyeLinkCBind
        Either a dummy object, or an EyeLink object.
    """
    # If we run in dummy mode, return early
    if dummy_mode:
        print("DUMMY MODE specified for eye-tracker. Will not track eyes.")
        tk = DummyEyeLink()
        return tk

    # Else, we need to import some libraries
    import pylink

    # And start the setup
    scn_w, scn_h = mon.getSizePix()

    tk = pylink.EyeLink("100.1.1.1")

    tk_version = tk.getTrackerVersion()
    assert tk_version == 3, "Expecting an Eyelink 1000 Plus"

    tvstr = tk.getTrackerVersionString()
    vindex = tvstr.find("EYELINK CL")
    host_version = int(float(tvstr[(vindex + len("EYELINK CL")) :].strip()))
    assert host_version == 5, "Expecting to work with version 5.x"

    assert (len(edf_fname) - len(".edf")) <= 8
    tk.openDataFile(edf_fname)
    tk.sendCommand("add_file_preamble_text 'RECORDED BY memcon'")

    tk.setOfflineMode()

    tk.sendMessage(f"DISPLAY_COORDS = 0 0 {scn_w - 1} {scn_h - 1}")
    tk.sendCommand("sample_rate 1000")
    tk.sendCommand(f"screen_pixel_coords = 0 0 {scn_w - 1} {scn_h - 1}")
    tk.sendCommand("recording_parse_type = GAZE")
    tk.sendCommand(f"calibration_type = {calibration_type}")
    tk.sendCommand("calibration_area_proportion 0.7 0.7")
    tk.sendCommand("validation_area_proportion  0.7 0.7")


    # 1. Nine-point calibration is a good standard calibration. It should always
    # be followed by a validation and both calibration and validation must
    # be GOOD. Participants should be instructed to look at the small black
    # point in the center of the fixation target, not just the fixation
    # target. For accurate calibration, it should be done at a moderate pace
    # (e.g., one fixation point per second), and participants themselves or
    # the experimenter confirm each fixation during calibration/validation
    # (instead of an automatic trigger).
    tk.sendMessage('calibration_type = HV9')

    # 2. To have a more reliable / easy to set up / precise calibration, we can
    # (and should) constrain the calibration area to the part of the screen
    # that participants will look at. My understanding is that in your task,
    # gaze will remain at the center of the screen throughout the trial. So
    # I suggest we constrain the calibration area to a reasonably-sized
    # quadratic area around the fixation location (screen center). The
    # values below are assuming a 16:9 screen and would use 50% of the
    # screen vertically and the 28.12% (50/16*9) horizontally.
    tk.sendMessage('calibration_area_proportion = 0.2812 0.5')

    # 3. We want binocular recordings to be able to look at microsaccades later
    # on, and we can determine that using the following commands. The first
    # two lines are for real-time samples through the link and the next two
    # lines regard writing of samples to the EDF file. Note that you will
    # still need to make sure that both eyes are recorded and used during
    # calibration, and to set up both eyes (adjusting position / blur etc.).
    tk.sendMessage('link_event_filter = LEFT,RIGHT,BUTTON')
    tk.sendMessage('link_sample_data = LEFT,RIGHT,GAZE,AREA')

    # 4. We want to have as little filtering / pre-processing of the data as
    # possible, but do need the heuristic filter to avoid large, hardware-
    # based artifacts in the recording. We will set both event and sample
    # fiters to 1.
    tk.sendMessage('heuristic_filter = 1 1')


    # Set what we record in the EDF file: Sample Filter, Event Data, Event Filter
    # like setFileSampleFilter (NOTE: file_sample_data is no typo!)
    tk.sendCommand("file_sample_data = GAZE,GAZERES,HREF,PUPIL,AREA,STATUS,INPUT")

    # like setFileEventData
    tk.sendCommand("file_event_data = GAZE,GAZERES,HREF,AREA,VELOCITY,STATUS")

    # like setFileEventFilter
    tk.sendCommand(
        "file_event_filter = LEFT,RIGHT,FIXATION,FIXUPDATE,SACCADE,BLINK,MESSAGE,INPUT"
    )

    return tk


def calibrate_eyetracker(tk, win):
    """Do setup and calibrate the EyeLink.

    A well documented version of this setup is described in the try_eyelink.py
    script in the eye_tracking/ directory.

    Parameters
    ----------
    tk : DummyEyeLink | EyeLinkCBind
        Either a dummy object, or an EyeLink object.
    win : psychopy.visual.Window
        The psychopy window on which to draw the stimuli.

    Returns
    -------
    tk : DummyEyeLink | EyeLinkCBind
        Either a dummy object, or an EyeLink object.
    """

    # return without calibration of no eyetracking is used
    if isinstance(tk, DummyEyeLink):
        return tk

    # Else, we need to import some libraries
    import pylink
    from EyeLinkCoreGraphicsPsychoPy import EyeLinkCoreGraphicsPsychoPy
    from psychopy import event


    # Do calibration
    text_stim = get_central_text_stim(
        win,
        height=TEXT_HEIGHT_DVA,
        text="Experiment\n\nEye-tracker Kalibrierung (press ENTER twice)",
    )
    text_stim.draw()

    genv = EyeLinkCoreGraphicsPsychoPy(tk, win)
    pylink.openGraphicsEx(genv)

    win.flip()
    event.waitKeys(keyList=["return"])

    while True:
        tk.doTrackerSetup()

        text_stim.text = (
            "Experiment\n\n(press ESC to re-do calibration)\n\n"
            "(press ENTER to accept calibration and proceed)"
        )
        text_stim.draw()
        win.flip()
        keys = event.waitKeys(keyList=["return", "esc"])

        if keys[0] == "return":
            break
        else:
            win.flip()

    text_stim.text = (
        "Experiment\n\nCalibration done (press any key to continue)."
    )
    text_stim.draw()
    win.flip()
    event.waitKeys()
    return tk


def check_drift(tk, win):
    """Perform a drift check with potential re-calibration.

    Press "return" to accept drift check.
    Press "esc" to reject drift check and enter setup mode.

    In setup mode, press "c" for calibration, or "v" for validation.
    In setup mode, press "esc" to re-do the drift check.

    Returns
    -------
    error : int
        0 if success, 27 if ESC was pressed to enter re-calibration.
    """
    error = 0
    if isinstance(tk, DummyEyeLink):
        return error

    import pylink

    scn_w, scn_h = win.monitor.getSizePix()
    while True:
        try:
            error = tk.doDriftCorrect(int(scn_w / 2), int(scn_h / 2), 1, 1)

            # break if successful drift check (no re-calibration), that is,
            # "enter " was pressed
            if error is not pylink.ESC_KEY:
                break
        except:  # noqa: E722 ... this is how it is done in EyeLink examples
            pass

    return error


def convert_edf2asc(command):
    """Call the EDF2ASC utility as a subprocess."""
    output = subprocess.run(command, shell=True, capture_output=True)
    stdout = output.stdout.decode("utf-8")
    if stdout:
        print("STD OUT\n------\n")
        print(stdout)
    stderr = output.stderr.decode("utf-8")
    if stderr:
        print("STD ERR\n------\n")
        print(stderr)
    return output


DUMMY_EYETRACK_DICT = dict(tk=DummyEyeLink(), edf_fname="", edf_fname_local="")
